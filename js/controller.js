/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
wimb.controller = {
    destinySelected: -1
};

//print the given elemnt
wimb.controller.print = function(object)
{
    console.log(object);
};

//call the get destinies data function,
// with the display destinies as parameter
wimb.controller.loadDestinies = function()
{
    wimb.model.getDestiniesArray(wimb.view.displayDestinies);
};

//call the get destinies data function,
// with the display destinies as parameter
wimb.controller.loadRoutes = function()
{
    if (this.destinySelected == -1) {
        
        alert("Debes seleccionar un destino");
        return;
    }
    
    wimb.model.getRoutesArray(this.destinySelected, wimb.view.displayNextRoute);
};
