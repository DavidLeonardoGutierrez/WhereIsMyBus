/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
wimb.view = {
    destiniesDropdown: "#destiniesDropdown"
};


//Get the selector
wimb.view.getSelector = function (selector)
{
    return $(selector);
};

//Build the html for the destinies
wimb.view.displayDestinies = function (data)
{
    var dropdown = wimb.view.getSelector(this.destiniesDropdown);

    //Clean the dropdown
    dropdown.empty();

    $.each(data, function (index, element) {

        dropdown.append(
                $('<li>').append(
                $('<a>')
                .attr('href', '#')
                .append(element.destiny)
                .click(function () {
                    
                    wimb.controller.destinySelected = element.id;
                    $("#textdropdown").text(element.destiny);
                })));
    });
};


//Build the html for the destinies
wimb.view.displayNextRoute = function (data)
{
    alert(data);
}