/* 

 * To change this license header, choose License Headers in Project Properties.

 * To change this template file, choose Tools | Templates

 * and open the template in the editor.

 */

wimb.model = {};



//Gets the url to the given endpoint

wimb.model.buildUrl = function (endPoint)

{

    return wimb.config.wsServer + wimb.config.wsFolder + endPoint;

};



//send an Ajax request

wimb.model.getAjaxCall = function (url, onSuccess)

{

    $.ajax({

        url: url,

        crossDomain: true,
        type:"GET",
        contentType: "application/json; charset=utf-8",

        success: function (data) {



            onSuccess($.parseJSON(data));

        },

        error: function () {

            console.log("Error")

        }

    });

};



//Gets the array of destinies

//AJAX GET CALL

wimb.model.getDestiniesArray = function (onSuccess)

{

    var c = wimb.config;

    var url = this.buildUrl(c.getDestiniesEP);

    this.getAjaxCall(url, onSuccess);

};



//Gets the array of routes

//AJAX GET CALL

wimb.model.getRoutesArray = function (destinyId, onSuccess)

{

    var c = wimb.config;

    var url = this.buildUrl(c.getRoutesEP + "/" + destinyId);

    this.getAjaxCall(url, onSuccess);

};	