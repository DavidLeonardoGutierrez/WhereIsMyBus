<?php



/*

 * To change this license header, choose License Headers in Project Properties.

 * To change this template file, choose Tools | Templates

 * and open the template in the editor.

 */



/**

 * Description of routes

 *

 * @author ho

 */

class RoutesModel extends CI_Model {



    //put your code here

    public function __construct() {

        // Call the CI_Model constructor

        parent::__construct();



        $this->load->database();

    }



    public function GetDestinies() {

        $query = $this->db->get('destiny');

        return $query->result();

    }



    public function GetRoutes($destinyId) {



        $this->db->where('destiny_id', $destinyId);

        $query = $this->db->get('route');

        return $query->result();

    }



}

