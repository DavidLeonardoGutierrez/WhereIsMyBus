<?php



/*

 * To change this license header, choose License Headers in Project Properties.

 * To change this template file, choose Tools | Templates

 * and open the template in the editor.

 */



/**

 * Description of routes

 *

 * @author ho

 */

class Routes extends CI_Controller {



    public function GetDestinies()

    {

        $this->load->model('RoutesModel');



        $data = $this->RoutesModel->GetDestinies();

        

        echo json_encode($data);

    }



    public function GetRoutes($destinyId)

    {

        $this->load->model('RoutesModel');



        $data = $this->RoutesModel->GetRoutes($destinyId);



        echo json_encode($data);

    }

}

